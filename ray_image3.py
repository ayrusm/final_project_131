""" 
    NAME : Suryashree Ray
    SECTION LEADER : Grace Chmielinski
    DATE : 25 April 2022
    ISTA 131 Final Project
    DESCRIPTION: In this program we try and create a plot for the
                 number of extra teresstial beings that visted earth
                 between certain latitudes. 
"""
import pandas as pd
import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
from datetime import datetime

def open_csvfile():
    '''
        This functions helps to open up the complete csv
        file.

        Parameters: None
        
        Return: The dataframe with the csv file
    '''
    df = pd.read_csv("complete.csv", infer_datetime_format=True, usecols= range(11))
    return df

def clean_data(df):
    '''
        This function helps to drop the rows with wierd
        dates. It helps to clean up the data a little bit.

        Parameters: df is the dataframe we take in
        
        Return: df2 is the dataframe with the cleaned up data
    '''
    # This is to find an drop the bad rows
    bad_rows = []
    for i, dt in enumerate(df.datetime):
        dtl = dt.split()
        if len(dtl) != 2:
            bad_rows.append(i)
    df2 = df.drop(bad_rows)

    # Correcting the range
    df2.index = range(len(df2))

    # Creating a column with datetime objects
    dts = []
    for i, dt in enumerate(df2.datetime):
        dtl = dt.split()
        dtl = dtl[0].split("/")
        dts.append(datetime(int(dtl[2]), int(dtl[0]), int(dtl[1])))

    df2.datetime = dts
    return df2

def plot_image_3(df):
    '''
        This function helps to draw the plot.

        Parameters: df is the dataframe we take in
        
        Return: None
    '''
    
    plt.rcParams['axes.facecolor'] = 'black'
    df.loc[: ,'latitude'].replace({0.0: np.nan}, inplace=True)
    latitude = df.latitude

    # Cleaning up latitude data
    for i, val in enumerate(latitude):
        if type(val) is str and "/" in val:
            val = float(df.longitude.iloc[i])
        elif type(val) is str:
            val = float(val)
        latitude[i] = val
    
    # Plotting the histogram
    bins = range(-90, 100, 10)
    plt.hist(latitude, bins = bins, edgecolor = "turquoise")
    plt.grid(True)
    plt.xticks(ticks = bins)
    plt.title('Count of Extraterrestial visits according to the latitudes', fontsize=10)
    plt.ylabel('Count')
    plt.xlabel("Latitude")

def main():
    df = open_csvfile()
    df = clean_data(df)
    plot_image_3(df)
    plt.show()

main()