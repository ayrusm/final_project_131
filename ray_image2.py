""" 
    NAME : Suryashree Ray
    SECTION LEADER : Grace Chmielinski
    DATE : 25 April 2022
    ISTA 131 Final Project
    DESCRIPTION: In this program we try and create a plot for the
                 number of extra teresstial beings that visted earth
                 according to the months. 
"""
import pandas as pd
import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
from datetime import datetime

def open_csvfile():
    '''
        This functions helps to open up the complete csv
        file.

        Parameters: None
        
        Return: The dataframe with the csv file
    '''
    df = pd.read_csv("complete.csv", infer_datetime_format=True, usecols= range(11))
    return df

def clean_data(df):
    '''
        This function helps to drop the rows with wierd
        dates. It helps to clean up the data a little bit.

        Parameters: df is the dataframe we take in
        
        Return: df2 is the dataframe with the cleaned up data
    '''
    # This is to find an drop the bad rows
    bad_rows = []
    for i, dt in enumerate(df.datetime):
        dtl = dt.split()
        if len(dtl) != 2:
            bad_rows.append(i)
    df2 = df.drop(bad_rows)

    # Correcting the range
    df2.index = range(len(df2))

    # Creating a column with datetime objects
    dts = []
    for i, dt in enumerate(df2.datetime):
        dtl = dt.split()
        dtl = dtl[0].split("/")
        dts.append(datetime(int(dtl[2]), int(dtl[0]), int(dtl[1])))

    # Creating a new column for months
    df2.datetime = dts
    df2['month']=df2['datetime'].dt.month
    return df2

def plot_image_2(df):
    '''
        This function helps to draw the plot.

        Parameters: df is the dataframe we take in
        
        Return: None
    '''
    # Plotting the bar plots
    plt.rcParams['axes.facecolor'] = 'black'
    month_plot = pd.DataFrame(df['month'].value_counts())
    month_plot = month_plot.sort_index()
    month_plot.index = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    color = ["red", "orange", "yellow", "chartreuse", "green", "springgreen",\
        "cyan", "dodgerblue", "blue", "purple","violet", "magenta"]
    fig, ax = plt.subplots()
    rects = ax.bar(data = month_plot, x = month_plot.index, height = month_plot["month"], color = color)

    # Annotating the plot
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1*height,
                '%d' % int(height),
                ha='center', va='bottom', color = "white")
                
    plt.grid(color='#95a5a6', linestyle='--', axis='y', alpha=0.7)
    plt.title('Count of Extraterrestial visits per month over the years', fontsize=10)
    plt.ylabel('Count')
    plt.xlabel("Month")

def main():
    df = open_csvfile()
    df = clean_data(df)
    plot_image_2(df)
    plt.show()

main()