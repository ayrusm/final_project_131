""" 
    NAME : Suryashree Ray
    SECTION LEADER : Grace Chmielinski
    DATE : 25 April 2022
    ISTA 131 Final Project
    DESCRIPTION: In this program we try and create a plot for the
                 number of extra teresstial beings that visted earth
                 between 1965 and 2015. 
"""
import pandas as pd
import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
from datetime import datetime

def open_csvfile():
    '''
        This functions helps to open up the complete csv
        file.

        Parameters: None
        
        Return: The dataframe with the csv file
    '''
    df = pd.read_csv("complete.csv", infer_datetime_format=True, usecols= range(11))
    return df

def clean_data(df):
    '''
        This function helps to drop the rows with wierd
        dates. It helps to clean up the data a little bit.

        Parameters: df is the dataframe we take in
        
        Return: df2 is the dataframe with the cleaned up data
    '''
    # This is to find an drop the bad rows
    bad_rows = []
    for i, dt in enumerate(df.datetime):
        dtl = dt.split()
        if len(dtl) != 2:
            bad_rows.append(i)
    df2 = df.drop(bad_rows)

    # Correcting the range
    df2.index = range(len(df2))

    # Creating a column with datetime objects
    dts = []
    for i, dt in enumerate(df2.datetime):
        dtl = dt.split()
        dtl = dtl[0].split("/")
        dts.append(datetime(int(dtl[2]), int(dtl[0]), int(dtl[1])))

    # Creating a new column for years
    df2.datetime = dts
    df2['year']=df2['datetime'].dt.year
    return df2

def plot_image_1(df):
    '''
        This function helps to draw the plot.

        Parameters: df is the dataframe we take in
        
        Return: None
    '''
    # Plotting the scatter plots
    plt.rcParams['axes.facecolor'] = 'black'
    yr_plot = pd.DataFrame(df['year'].value_counts().head(50)).reset_index()
    yr_plot.index = yr_plot['index']
    yr_plot.drop("index", axis = 1)
    ax = plt.scatter(x=yr_plot['index'], y=yr_plot['year'], c ="turquoise", linewidths = 2, marker ="o", edgecolor ="green", s = 30)
    plt.title('Count of Extraterrestial visits to earth between 1965 and 2015(acc. data)', fontsize=10)
    plt.ylabel('Count')
    plt.xlabel("Year")

    # Plotting the best fit line
    x = np.arange(1965,2015)
    params  = get_ols_parameters(yr_plot["year"])
    y = params[0]*x + params[1]
    plt.plot(x, y, c = "purple")


    
def get_ols_parameters(s):
    '''
        This functions makes takes a series and creates a list
        of slope, y_intercept, r_2, p by using the ols function
        of statsmodel.

        Parameters: s which is a series
        
        Return: a list of stuff mentioned above
    '''

    index = s.index.values
    indx = sm.add_constant(index)
    model = sm.OLS(s, indx)
    results = model.fit()

    # Finding the different parts to include in the list
    slope = results.params["x1"]
    y_intercept = results.params["const"]
    r_2 = results.rsquared
    p = results.pvalues["x1"]

    return [slope, y_intercept, r_2, p]

def main():
    df = open_csvfile()
    df = clean_data(df)
    plot_image_1(df)
    plt.show()

main()